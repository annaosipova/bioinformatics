package BA8A;

import com.rosalind.BA8A.FarthestFirstTraversal;
import com.rosalind.Utils;
import edu.princeton.cs.algs4.In;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.util.*;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */
@RunWith(value = Parameterized.class)
public class FarthestFirstTraversalTest {
    private File inputFile;
    private File outputFile;
    private int k;
    private int m;
    private List<double[]> data;
    private List<double[]> output;

    public FarthestFirstTraversalTest(String inputFile, String outputFile) {
        this.inputFile = new File(inputFile);
        this.outputFile = new File(outputFile);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        String filePath = "src/tests/resources/BA8A/";
        Object[][] data = new Object[][]{
                {filePath + "input1", filePath + "output1"},
                {filePath + "input2", filePath + "output2"}
        };
        return Arrays.asList(data);
    }

    @Before
    public void setUp() throws Exception {
        In in = new In(inputFile);
        String[] line = in.readLine().split("\\s");
        m = Integer.parseInt(line[1]);
        k = Integer.parseInt(line[0]);
        data = new ArrayList<>();
        Utils.loadDataToList(in, m, data);

        output = new ArrayList<>(k);
        in = new In(outputFile);
        Utils.loadDataToList(in, m, output);
    }

    @Test
    public void testGetCenters() throws Exception {
        FarthestFirstTraversal fft = new FarthestFirstTraversal(data, k, m);
        List<double[]> centers = fft.getCenters();
        for (int i = 0; i < centers.size(); i++) {
            Assert.assertArrayEquals(centers.get(i), output.get(i), 0.001);
        }
    }
}