package BA8B;

import com.rosalind.BA8B.SquaredErrorDistortion;
import com.rosalind.Utils;
import edu.princeton.cs.algs4.In;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class SquaredErrorDistortionTest {
    private File centersFile;
    private File dataPointsFile;
    private File outputFile;
    private int m;
    private List<double[]> dataPoints;
    private List<double[]> centers;
    private double output;

    public SquaredErrorDistortionTest(String dirname) {
        String filePath = "src/tests/resources/BA8B/";
        String centersPath = "centers";
        String pointsPath = "dataPoints";
        String outputPath = "output";
        this.centersFile = new File(filePath + dirname + "/" + centersPath);
        this.dataPointsFile = new File(filePath + dirname + "/" + pointsPath);
        this.outputFile = new File(filePath + dirname + "/" + outputPath);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {"input1"},
                {"input2"}
        };
        return Arrays.asList(data);
    }

    @Before
    public void setUp() throws Exception {
        In in = new In(centersFile);
        String[] line = in.readLine().split("\\s");
        int k = Integer.parseInt(line[0]);
        m = Integer.parseInt(line[1]);
        centers = new ArrayList<>(k);
        Utils.loadDataToList(in, m, centers);

        in = new In(dataPointsFile);
        dataPoints = new ArrayList<>();
        Utils.loadDataToList(in, m, dataPoints);

        in = new In(outputFile);
        output = Double.parseDouble(in.readLine());
        in.close();
    }

    @Test
    public void testDistortion() throws Exception {
        SquaredErrorDistortion sed = new SquaredErrorDistortion(dataPoints, centers, m);
        assertEquals(sed.distortion(), output, 0.01);
    }
}