package tests.BA5M;

import com.rosalind.BA5M.HighScoringMultipleAligment;
import edu.princeton.cs.algs4.In;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class TestHighestScoringMultipleSequenceAlignment {
    private static String file = "src/tests/resources/BA5M";
    private int output;
    private int currentOutput;
    private File inputFile;
    private File outputFile;

    public TestHighestScoringMultipleSequenceAlignment(String input, String output){
        this.outputFile = new File(output);
        this.inputFile =new File(input);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        Object[][] data = new Object[][]{
                {file + "input", file + "output"}
        };
        return Arrays.asList(data);
    }

    @Before
    public void readFiles(){
        In in = new In(inputFile);
        String s1 = in.readLine();
        String s2 = in.readLine();
        String s3 = in.readLine();
        currentOutput = Integer.parseInt(HighScoringMultipleAligment.aligment(s1, s2, s3).split("\n")[0]);

        in = new In(outputFile);
        output = in.readInt();
    }

    @Test
    public void Test(){
        Assert.assertEquals(output, currentOutput);
    }
}
