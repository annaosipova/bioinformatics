package com.rosalind.BA4G;

import com.rosalind.Amino;
import com.rosalind.BA4C.TheoreticalSpectrum;

import java.util.*;

public class LeadBoard {
    private static Map<Character, Integer> weights = Amino.loadMasses();
    private List<String> leaderBoard;
    private static char[] aminos = {'G', 'A', 'S', 'P', 'V', 'T', 'C', 'I', 'L', 'N', 'D', 'K', 'Q', 'E', 'M', 'H','F', 'R', 'Y', 'W'};

    public LeadBoard(){
        leaderBoard = new ArrayList<>();
    }

    public String leaderboardCyclopeptydeSequencing(String spectrum, int N){
        String leaderPeptyde = "";
        leaderBoard.add(leaderPeptyde);
        String[] split = spectrum.split(" ");
        int parentMass = Integer.parseInt(split[split.length-1]);

        while (!leaderBoard.isEmpty()){
            leaderBoard = expand();
            List<String> newBoard = new ArrayList(leaderBoard);
            for (String peptyde : leaderBoard) {
                int mass = mass(peptyde);
                if (mass == parentMass){
                    if (TheoreticalSpectrum.score(peptyde,spectrum) > TheoreticalSpectrum.score(leaderPeptyde,spectrum)){
                        leaderPeptyde = peptyde;
                    }
                }else if (mass > parentMass)
                    newBoard.remove(peptyde);
            }
            leaderBoard = newBoard;
            cut(spectrum, N);
        }
        return delimetedString(leaderPeptyde);
    }

    private List<String> expand(){
        List<String> expandBoard = new ArrayList<>();
        for (String peptyde : leaderBoard){
            for (char amino : aminos){
                expandBoard.add(peptyde+amino);
            }
        }
        return expandBoard;
    }

    private int mass(String peptyde){
        if (peptyde.length() == 0) return 0;

        int sum = 0;
        for (char ch : peptyde.toCharArray()){
            sum += weights.get(ch);
        }
        return sum;
    }

    private void cut(String spectrum, int N){
        if (leaderBoard.size() < N) return;

        List<String> newBoard = new ArrayList<>();
        Collections.sort(leaderBoard, (o1, o2) -> - TheoreticalSpectrum.score(o1,spectrum).compareTo(TheoreticalSpectrum.score(o2,spectrum)));

        int nscore = TheoreticalSpectrum.score(leaderBoard.get(N-1),spectrum);
        for (int i = 0; i < leaderBoard.size(); i++) {
            if (i < N)
                newBoard.add(leaderBoard.get(i));
            else if (nscore == TheoreticalSpectrum.score(leaderBoard.get(i),spectrum))
                newBoard.add(leaderBoard.get(i));
            else
                break;
        }

        leaderBoard = newBoard;
    }

    private String delimetedString(String peptyde){
        String res = "";
        char[] chars = peptyde.toCharArray();
        for (int i = chars.length - 1; i >= 0 ; i--) {
            res += weights.get(chars[i])+"-";
        }
        return res.substring(0, res.length() - 1);
    }


    public static void main(String[] args) {
        LeadBoard lb = new LeadBoard();
        int N = 325;
//        String spectrum = "0 71 87 99 101 103 103 114 128 129 137 147 156 156 172 201 216 217 218 231 236 240 255 257 257 259 303 304 319 328 330 339 344 356 360 368 373 374 392 406 427 433 447 458 460 467 475 475 477 493 495 497 520 561 561 564 574 578 584 589 591 596 596 607 623 631 664 667 678 683 692 698 711 717 724 730 734 736 752 779 795 797 801 807 814 820 833 839 848 853 864 867 900 908 924 935 935 940 942 947 953 957 967 970 970 1011 1034 1036 1038 1054 1056 1056 1064 1071 1073 1084 1098 1104 1139 1157 1158 1163 1171 1175 1187 1192 1201 1203 1212 1227 1228 1272 1274 1274 1276 1291 1295 1300 1313 1314 1315 1330 1359 1375 1375 1384 1394 1402 1403 1417 1428 1428 1430 1432 1444 1460 1531";
       String spectrum = "0 71 71 71 87 97 97 99 101 103 113 113 114 115 128 128 129 137 147 163 163 170 184 184 186 186 190 211 215 226 226 229 231 238 241 244 246 257 257 276 277 278 299 300 312 316 317 318 318 323 328 340 343 344 347 349 356 366 370 373 374 391 401 414 414 415 419 427 427 431 437 441 446 453 462 462 462 470 472 502 503 503 511 515 529 530 533 533 540 543 547 556 559 569 574 575 584 590 600 600 604 612 616 617 630 640 640 643 646 648 660 671 683 684 687 693 703 703 719 719 719 729 730 731 737 740 741 745 747 754 774 780 784 790 797 800 806 818 826 827 832 833 838 846 846 847 850 868 869 877 884 889 893 897 903 908 913 917 930 940 947 956 960 960 961 964 965 966 983 983 985 1002 1009 1010 1011 1021 1031 1031 1036 1053 1054 1058 1059 1062 1063 1074 1076 1084 1092 1103 1113 1122 1124 1130 1133 1134 1145 1146 1146 1149 1150 1155 1156 1171 1173 1174 1187 1191 1193 1200 1212 1221 1233 1240 1242 1246 1259 1260 1262 1277 1278 1283 1284 1287 1287 1288 1299 1300 1303 1309 1311 1320 1330 1341 1349 1357 1359 1370 1371 1374 1375 1379 1380 1397 1402 1402 1412 1422 1423 1424 1431 1448 1450 1450 1467 1468 1469 1472 1473 1473 1477 1486 1493 1503 1516 1520 1525 1530 1536 1540 1544 1549 1556 1564 1565 1583 1586 1587 1587 1595 1600 1601 1606 1607 1615 1627 1633 1636 1643 1649 1653 1659 1679 1686 1688 1692 1693 1696 1702 1703 1704 1714 1714 1714 1730 1730 1740 1746 1749 1750 1762 1773 1785 1787 1790 1793 1793 1803 1816 1817 1821 1829 1833 1833 1843 1849 1858 1859 1864 1877 1886 1890 1893 1900 1900 1903 1904 1918 1922 1930 1930 1931 1961 1963 1971 1971 1971 1980 1987 1992 1996 2002 2006 2006 2014 2018 2019 2019 2032 2042 2059 2060 2063 2067 2077 2084 2086 2089 2090 2093 2105 2110 2115 2115 2116 2117 2121 2133 2134 2155 2156 2157 2176 2176 2187 2189 2192 2195 2202 2204 2207 2207 2218 2222 2243 2247 2247 2249 2249 2263 2270 2270 2286 2296 2304 2305 2305 2318 2319 2320 2320 2330 2332 2334 2336 2336 2346 2362 2362 2362 2433";
        System.out.println(lb.leaderboardCyclopeptydeSequencing(spectrum, N));
    }
}
