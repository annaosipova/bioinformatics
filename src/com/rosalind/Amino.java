package com.rosalind;

import edu.princeton.cs.algs4.In;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */
public class Amino {
    private static File file = new File("amino_masses");;

    public static Map loadMasses(){
        Map<Character, Integer> masses = new HashMap<>();
        In in  = new In(file);

        while(!in.isEmpty()) {
            String[] line = in.readLine().split(" ");
            Character amino = line[0].toCharArray()[0];
            int mass = Integer.parseInt(line[1]);
            masses.put(amino,mass);
        }

        return masses;
    }

    public static int[] masses(){
        Map<Character, Integer> map = loadMasses();
        int[] masses = new int[map.entrySet().size()];
        int i = 0;
        for (Map.Entry<Character, Integer> entry : map.entrySet()){
            masses[i] = entry.getValue();
            i++;
        }
        return masses;
    }

    public static char[] aminos(){
        Map<Character, Integer> map = loadMasses();
        char[] aminos = new char[map.entrySet().size()];
        int i = 0;
        for (Character key : map.keySet()){
            aminos[i] = key;
            i++;
        }
        return aminos;
    }

    public static Map loadPepMasses(){
        Map<Integer, String> masses = new HashMap<>();
        In in  = new In(file);

        while(!in.isEmpty()) {
            String[] line = in.readLine().split(" ");
            masses.put(Integer.parseInt(line[1]),line[0]);
        }

        return masses;
    }
}
