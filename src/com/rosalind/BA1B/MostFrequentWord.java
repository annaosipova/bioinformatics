package com.rosalind.BA1B;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MostFrequentWord {
    private String genome;

    public MostFrequentWord(String genome){
        this.genome = genome;
    }

    public String mostFrequent(int k){
        ArrayList<String> matrix = new ArrayList<String>();
        Map<String,Integer> frecuency = new HashMap<String,Integer>();

        int max = 0;
        String res = "";

        for (int i = 0; i < genome.length()-k; i++){
            matrix.add(genome.substring(i,i+k));
        }

        Collections.sort(matrix);

        for (String s : matrix){
            if (frecuency.keySet().contains(s)){
                frecuency.put(s,frecuency.get(s)+1);
            }
            else frecuency.put(s,1);
        }

        for (String s : frecuency.keySet()){
            if (max < frecuency.get(s))
                max = frecuency.get(s);
        }

        for (String s : frecuency.keySet()){
            if (frecuency.get(s) == max)
                res +=s+" ";
        }
        return res;
    }

    public String clumps(int k, int L, int t){


        return null;
    }

    public static void main(String[] args) {
        String genome = "GCAGCGCGGCATTCTAGATGGTAATCGATGGTAATCGATGGTAATCCAACCACCTACAACCACCTAGCAGCGCGGCATTCTAAGCTGATCCGATGGTAATCCAACCACCTAAGCTGATCCAGCTGATCCGATGGTAATCAGCTGATCCGCATTCTAGCAGCGCGAGCTGATCCGCATTCTAGCATTCTAGATGGTAATCGCAGCGCGGCAGCGCGGATGGTAATCGCAGCGCGGCATTCTACAACCACCTAGATGGTAATCGATGGTAATCGCAGCGCGGCATTCTAGATGGTAATCGCAGCGCGGCATTCTACAACCACCTAGATGGTAATCGCAGCGCGGCATTCTAGCAGCGCGGCAGCGCGGCATTCTAGCAGCGCGGATGGTAATCGATGGTAATCGCATTCTAGCAGCGCGAGCTGATCCCAACCACCTAGATGGTAATCGCAGCGCGGCATTCTAAGCTGATCCGCAGCGCGGCATTCTACAACCACCTAGCATTCTAGCATTCTAAGCTGATCCGCATTCTACAACCACCTAGCAGCGCGGCATTCTAGATGGTAATCCAACCACCTAGATGGTAATCGCAGCGCGGCATTCTAGCATTCTAAGCTGATCCGATGGTAATCCAACCACCTAGCAGCGCGCAACCACCTACAACCACCTACAACCACCTAGATGGTAATCGCAGCGCGGCATTCTAAGCTGATCCGCAGCGCGGATGGTAATCGATGGTAATCGCAGCGCGGATGGTAATCAGCTGATCCAGCTGATCCGATGGTAATCAGCTGATCCGCAGCGCGAGCTGATCCGATGGTAATCCAACCACCTAGCATTCTAAGCTGATCCGCATTCTAAGCTGATCCCAACCACCTAAGCTGATCCGATGGTAATCGCAGCGCGGATGGTAATCGATGGTAATCGATGGTAATCAGCTGATCCGCAGCGCGGATGGTAATCAGCTGATCCGCAGCGCGGCATTCTAAGCTGATCC";
        MostFrequentWord mfw = new MostFrequentWord(genome);
        System.out.println(mfw.mostFrequent(11));
    }

}
