package com.rosalind;

import edu.princeton.cs.algs4.In;

import java.util.Arrays;
import java.util.List;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Utils {

    public static void loadDataToList(In in, int m, List<double[]> dst) {
        String[] line;
        while (!in.isEmpty()){
            line = in.readLine().split("\\s");
            double[] point = new double[m];
            int i = 0;
            for (String coordinate : line) {
                point[i++] = Double.parseDouble(coordinate);
            }
            dst.add(point);
        }
    }

    public static double EuclideanDistance(List<double[]> points, int dimension, double[] point){
        double[] distances = new double[points.size()];
        int k = 0;
        for (double[] srcPoint : points) {
            double sum = 0;
            for (int i = 0; i < dimension; i++) {
                sum += pow(srcPoint[i] - point[i], 2);
            }
            distances[k++] = sqrt(sum);
        }
        return Arrays.stream(distances).min().getAsDouble();
    }

    public static double pointDistance(double[] point1, double[] point2, int dimension) {
        double sum = 0;
        for (int i = 0; i < dimension; i++) {
            sum += pow(point1[i] - point2[i], 2);
        }
        return sqrt(sum);
    }
}
