package com.rosalind.BA5M;

public class HighScoringMultipleAligment {
    public static String aligment(String s1, String s2, String s3) {
        int i = s1.length();
        int j = s2.length();
        int k = s3.length();
        int[][][] backtrack = matrix(s1, s2, s3);

        while (i != 0 && j != 0 && k != 0) {
            switch (backtrack[i][j][k]){
                case 0:
                    i--;
                    s2 = dash(s2, j);
                    s3 = dash(s3, k);
                    break;
                case 1:
                    j--;
                    s1 = dash(s1, i);
                    s3 = dash(s3, k);
                    break;
                case 2:
                    k--;
                    s1 = dash(s1, i);
                    s2 = dash(s2, j);
                    break;
                case 3:
                    i--;
                    j--;
                    s3 = dash(s3, k);
                    break;
                case 4:
                    i--;
                    k--;
                    s2 = dash(s2, j);
                    break;
                default:
                    i--;
                    j--;
                    k--;

            }
        }
        int maxLength = Math.max(s1.length(), s2.length());
        maxLength = Math.max(maxLength, s3.length());

        while (s1.length() != maxLength)
            s1 = dash(s1, 0);
        while (s2.length() != maxLength)
            s2 = dash(s2, 0);
        while (s3.length() != maxLength)
            s3 = dash(s3, 0);

        return backtrack[0][0][0] + "\n" + s1 + "\n" + s2 + "\n" + s3;
    }

    public static int[][][] matrix(String s1, String s2, String s3) {
        int[][][] grid = new int[s1.length() + 1][s2.length() + 1][s3.length() + 1];
        int[][][] backtrack = new int[s1.length() + 1][s2.length() + 1][s3.length() + 1];
        char[] ch1 = s1.toCharArray();
        char[] ch2 = s2.toCharArray();
        char[] ch3 = s3.toCharArray();

        for (int i = 1; i < s1.length() + 1; i++) {
            for (int j = 1; j < s2.length() + 1; j++) {
                for (int k = 1; k < s3.length() + 1; k++) {
                    int mismatch = (ch1[i - 1] == ch2[j - 1] && ch2[j - 1] == ch3[k - 1]) ? 1 : 0;
                    int[] scores = {grid[i - 1][j][k],
                            grid[i][j - 1][k],
                            grid[i][j][k - 1],
                            grid[i - 1][j - 1][k],
                            grid[i][j - 1][k - 1],
                            grid[i - 1][j][k - 1],
                            grid[i - 1][j - 1][k - 1] + mismatch};
                    int max = 0;
                    for (int l = 0; l < scores.length; l++) {
                        if (scores[l] > max) {
                            max = scores[l];
                            backtrack[i][j][k] = l;
                        }
                    }
                    grid[i][j][k] = max;
                }
            }
        }
        backtrack[0][0][0] = grid[s1.length()][s2.length()][s3.length()];
        return backtrack;
    }

    private static String dash(String str, int index) {
        String sub1 = str.substring(0, index);
        String sub2 = str.substring(index, str.length());
        return sub1 + "-" + sub2;
    }
}
