package com.rosalind.BA7A;


public class DistanceBetweenLeaves {

    private int v;
    private int[][] dist;
    public int[][] weights;
    int[] degree;
    private String[] adjacency;

    public DistanceBetweenLeaves(int v, String[] adjacency) {
        this.v = v;
        this.adjacency = adjacency;
        initDist();
        weights = new int[v][v];
        degree = new int[v];
        constructTree();
    }

    private void initDist() {
        dist = new int[v][v];
        for (int i = 0; i < v; i++) {
            for (int j = 0; j < v; j++) {
                dist[i][j] = Integer.MAX_VALUE;
            }
        }
    }

    private void constructTree() {
        for (String s : adjacency) {
            String[] split = parse(s);
            int from = Integer.parseInt(split[0]);
            int to = Integer.parseInt(split[1]);
            int weight = Integer.parseInt(split[2]);
            weights[from][to] = weight;
            degree[to]++;
        }
    }

    private String[] parse(String s) {
        return s.replaceAll("->", " ").replaceAll(":", " ").split("\\s");
    }

    public int[][] getDistMatrix() {
        for (int i = 0; i < v; i++) {
            dist[i][i] = 0;
        }

        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[i].length; j++) {
                if (degree[j] > 1) continue;
                dist[i][j] = weights[i][j];
            }
        }

        for (int k = 1; k < v; k++) {
            for (int i = 1; i < v; i++) {
                for (int j = 1; j < v; j++) {
                    if (dist[i][j] > dist[i][k] + dist[k][j]){
                        dist[i][j] = dist[i][k] + dist[k][j];
                    }
                }
            }
        }
        return dist;
    }

    public static void main(String[] args) {
        int n = 4;
        String[] adj = new String[]{
                "0->4:11", "1->4:2", "2->5:6",
                "3->5:7", "4->0:11", "4->1:2",
                "4->5:4", "5->4:4", "5->3:7",
                "5->2:6"
        };
        DistanceBetweenLeaves dbl = new DistanceBetweenLeaves(n + 2, adj);
        int[][] distMat = dbl.weights;
        for (int[] row : distMat){
            for (int i : row){
                System.out.print(i+" ");
            }
            System.out.println();
        }
    }
}
