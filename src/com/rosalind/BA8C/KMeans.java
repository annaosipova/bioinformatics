package com.rosalind.BA8C;

import com.rosalind.Utils;
import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */
public class KMeans {

    private List<double[]> points;
    private int k;
    private int m;
    private List<Cluster> clusters;
    private List<double[]> centers;

    public KMeans(List<double[]> points, int k, int m) {
        this.points = points;
        this.k = k;
        this.m = m;
        clusters = new ArrayList<>(k);
        centers = new ArrayList<>(k);
        init();
        calculate();
    }

    private void init() {
        for (int i = 0; i < k; i++) {
            Cluster cluster = new Cluster(points.get(i));
            centers.add(points.get(i));
            clusters.add(cluster);
        }
    }

    private void calculate() {
        int distance = Integer.MAX_VALUE;

        while (distance != 0) {
            distance = 0;
            clearClusters();
            List<double[]> lastCenters = copyCenters();
            lastCenters.addAll(centers);
            assignPoints();
            calculateCenters();
            for (int j = 0; j < k; j++) {
                distance += Utils.pointDistance(lastCenters.get(j), centers.get(j), m);
            }
        }
    }

    private List<double[]> copyCenters() {
        List<double[]> newCenters = new ArrayList<>(k);
        for (double[] point : centers) {
            double[] cp = new double[m];
            System.arraycopy(point, 0, cp, 0, m);
            newCenters.add(cp);
        }
        return newCenters;
    }

    private void assignPoints() {
        int cluster = 0;

        for (double[] point : points){
            double min = Double.MAX_VALUE;
            for (int i = 0; i < k; i++) {
                Cluster cl = clusters.get(i);
                double distance = Utils.pointDistance(point, cl.getCenter(), m);
                if (distance < min) {
                    min = distance;
                    cluster = i;
                }
            }
            clusters.get(cluster).addPoint(point);
        }
    }

    private void calculateCenters() {
        centers.clear();
        for (Cluster cluster : clusters) {
            double[] sums = new double[m];
            List<double[]> points = cluster.getPoints();
            int clusterSize = points.size();

            for (double[] point : points) {
                for (int i = 0; i < m; i++) {
                    sums[i] += point[i];
                }
            }

            double[] center = cluster.getCenter();
            for (int i = 0; i < m; i++) {
                double coordinate = sums[i] / clusterSize;
                center[i] = coordinate;
            }
            cluster.setCenter(center);
            centers.add(center);
        }
    }

    private void clearClusters() {
        clusters.forEach(Cluster::clear);
    }

    private List<double[]> getCenters() {
        List<double[]> centers = new ArrayList<>(k);
        double[] copy = new double[m];
        for (Cluster cluster : clusters) {
            double[] old = cluster.getCenter();
            for (int i = 0; i < m; i++){
                double c = old[i];
                copy[i] = c;
            }
            centers.add(copy);
        }
        return centers;
    }

    public List<double[]> centers() {
        return centers;
    }

    public static void main(String[] args) {
        In in = new In("src/tests/resources/BA8C/input1");
        String[] line = in.readLine().split("\\s");
        int m = Integer.parseInt(line[1]);
        int k = Integer.parseInt(line[0]);
        List<double[]> data = new ArrayList<>();
        Utils.loadDataToList(in, m, data);

        KMeans means = new KMeans(data, k, m);
        for (double[] point : means.centers()) {
            for (int i = 0; i < m; i++) {
                System.out.print(point[i] + " ");
            }
            System.out.println();
        }
    }
}
