package com.rosalind.BA8C;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */
public class Cluster {

    List<double[]> points;
    double[] center;

    public Cluster(double[] center) {
        this.center = center;
        points = new ArrayList<>();
    }

    public void addPoint(double[] point) {
        points.add(point);
    }

    public void setCenter(double[] center) {
        this.center = center;
    }

    public double[] getCenter() {
        return center;
    }

    public List<double[]> getPoints() {
        return points;
    }

    public void clear() {
        points.clear();
    }
}
