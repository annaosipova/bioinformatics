package com.rosalind.BA4C;

import com.rosalind.Amino;

import java.util.*;
import java.util.stream.Collectors;

public class TheoreticalSpectrum {
    private static Map<Character, Integer> weights = Amino.loadMasses();

    private static List<String> combine(String peptyde){
        List<String> subpeptydes = new ArrayList<>();
        for (int i = 0; i < peptyde.length(); i++){
            for (int j = i; j < peptyde.length(); j++) {
                subpeptydes.add(peptyde.substring(i, j + 1));
            }
        }

        for (int i = 2; i < peptyde.length(); i++) {
            for (int j = 0; j < i - 1; j++) {
                subpeptydes.add(peptyde.substring(i,peptyde.length()) + peptyde.substring(0, j + 1));
            }
        }
        return subpeptydes;
    }

    public static List<Integer> cyclospectrum(String peptyde){
        List<Integer> mass = new ArrayList(){{add(0);}};

        for (String s : combine(peptyde)){
            char[] chars  = s.toCharArray();
            int sum = 0;
            for (char ch : chars){
                sum += weights.get(ch);
            }
            mass.add(sum);
        }
        Collections.sort(mass);

        return mass;
    }

    public static Integer score(String peptyde, String spectrum){
        int count = 0;
        List<Integer> cyclocpec = cyclospectrum(peptyde);
        String[] spec = spectrum.split(" ");
        int i = 0;
        int j = 0;
        while (i < spec.length && j < cyclocpec.size()){
            if (Integer.parseInt(spec[i]) == cyclocpec.get(j)){
                count++;
                i++;
                j++;
            }else if (Integer.parseInt(spec[i]) > cyclocpec.get(j)){
                j++;
            }else {
                i++;
            }
        }

        return count;
    }

    public static void main(String[] args) {
        String peptyde = "NQEL";
        String spec = "0 99 113 114 128 227 257 299 355 356 370 371 484";
        System.out.println(score(peptyde,spec));
    }
}
