package com.rosalind.BA8A;

import com.rosalind.Utils;

import java.util.*;

/**
 * @author Anna Osipova Anna_Osipova@epam.com
 */
public class FarthestFirstTraversal {
    private final List<double[]> dataPoints;
    private final List<double[]> centers;
    private final int k;
    private final int m;

    public FarthestFirstTraversal(List<double[]> data, int k, int m) {
        this.dataPoints = data;
        this.k = k;
        this.m = m;
        centers = new ArrayList<>(k);
        findCenters();
    }

    public void findCenters() {
        centers.add(dataPoints.get(0));
        while (centers.size() < k ) {
            NavigableMap<Double,double[]> minDistances = new TreeMap<>();
            for (double[] point : dataPoints) {
                minDistances.put(Utils.EuclideanDistance(centers, m, point), point);
            }
            centers.add(minDistances.lastEntry().getValue());
        }
    }

    public List<double[]> getCenters(){
        return centers;
    }
}
