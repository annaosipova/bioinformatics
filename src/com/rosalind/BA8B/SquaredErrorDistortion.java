package com.rosalind.BA8B;

import com.rosalind.Utils;
import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.List;

import static com.rosalind.Utils.EuclideanDistance;
import static java.lang.Math.pow;

public class SquaredErrorDistortion {

    private final List<double[]> dataPoints;
    private final List<double[]> centers;
    private final int m;

    public SquaredErrorDistortion(List<double[]> dataPoints, List<double[]> centers, int m) {
        this.dataPoints = dataPoints;
        this.centers = centers;
        this.m = m;
    }

    public double distortion() {
        double sum = 0;
        for (double[] point : dataPoints) {
            sum += pow(EuclideanDistance(centers, m, point), 2);
        }
        return sum / dataPoints.size();
    }

    public static void main(String[] args) {
        List<double[]> dataPoints;
        List<double[]> centers;

        In in = new In("src/tests/resources/BA8B/input3/centers");
        String[] line = in.readLine().split("\\s");
        int k = Integer.parseInt(line[0]);
        int m = Integer.parseInt(line[1]);
        centers = new ArrayList<>(k);
        Utils.loadDataToList(in, m, centers);

        in = new In("src/tests/resources/BA8B/input3/dataPoints");
        dataPoints = new ArrayList<>();
        Utils.loadDataToList(in, m, dataPoints);

        SquaredErrorDistortion sed = new SquaredErrorDistortion(dataPoints, centers, m);
        System.out.println(sed.distortion());
    }

}
